package com.example.digikalaapp.model;

public class CircleItem {

    int image;
    String name;

    public int getImage() {
        return image;
    }

    public CircleItem(int image, String name) {
        this.image = image;
        this.name = name;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
