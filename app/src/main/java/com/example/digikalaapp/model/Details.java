package com.example.digikalaapp.model;

import java.util.List;

public class Details {
    private String image;
    private String name;
    private List<SimProduct> imageList;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SimProduct> getImageList() {
        return imageList;
    }

    public void setImageList(List<SimProduct> imageList) {
        this.imageList = imageList;
    }
}
