package com.example.digikalaapp.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class OfferOne {

    private int id;
    private String image;
    private String desc;
    private String percent;
    private int price;
    private int pre_price;


    @BindingAdapter("android:LoadImage")
    public static void getImage(ImageView view , String url){
        Glide.with(view.getContext()).load(url).into(view);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPre_price() {
        return pre_price;
    }

    public void setPre_price(int pre_price) {
        this.pre_price = pre_price;
    }
}
