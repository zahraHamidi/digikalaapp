package com.example.digikalaapp.model;

import java.util.List;

public class ProductDetails {
    String name;
    String title;
    String titleTwo;
    String titleThree;
    String titleFour;
    String titleFive;
    String titleSix;
    List<Features> details;
    List<Features> detailsTwo;
    List<Features> detailsThree;
    List<Features> detailsFour;
    List<Features> detailsFive;
    List<Features> detailsSix;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Features> getDetails() {
        return details;
    }

    public void setDetails(List<Features> details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitleTwo() {
        return titleTwo;
    }

    public void setTitleTwo(String titleTwo) {
        this.titleTwo = titleTwo;
    }

    public String getTitleThree() {
        return titleThree;
    }

    public void setTitleThree(String titleThree) {
        this.titleThree = titleThree;
    }

    public String getTitleFour() {
        return titleFour;
    }

    public void setTitleFour(String titleFour) {
        this.titleFour = titleFour;
    }

    public String getTitleFive() {
        return titleFive;
    }

    public void setTitleFive(String titleFive) {
        this.titleFive = titleFive;
    }

    public String getTitleSix() {
        return titleSix;
    }

    public void setTitleSix(String titleSix) {
        this.titleSix = titleSix;
    }

    public List<Features> getDetailsTwo() {
        return detailsTwo;
    }

    public void setDetailsTwo(List<Features> detailsTwo) {
        this.detailsTwo = detailsTwo;
    }

    public List<Features> getDetailsThree() {
        return detailsThree;
    }

    public void setDetailsThree(List<Features> detailsThree) {
        this.detailsThree = detailsThree;
    }

    public List<Features> getDetailsFour() {
        return detailsFour;
    }

    public void setDetailsFour(List<Features> detailsFour) {
        this.detailsFour = detailsFour;
    }

    public List<Features> getDetailsFive() {
        return detailsFive;
    }

    public void setDetailsFive(List<Features> detailsFive) {
        this.detailsFive = detailsFive;
    }

    public List<Features> getDetailsSix() {
        return detailsSix;
    }

    public void setDetailsSix(List<Features> detailsSix) {
        this.detailsSix = detailsSix;
    }
}
