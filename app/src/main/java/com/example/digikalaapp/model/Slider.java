package com.example.digikalaapp.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class Slider {

   private int id;
   private String image;

   @BindingAdapter("android:LoadImage")
   public static void getImage(ImageView view , String url){
       Glide.with(view.getContext()).load(url).into(view);
   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
