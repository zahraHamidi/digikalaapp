package com.example.digikalaapp.network;

import android.service.autofill.ImageTransformation;

import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.model.CategoryTwo;
import com.example.digikalaapp.model.Details;
import com.example.digikalaapp.model.Introduce;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.model.PopularSearch;
import com.example.digikalaapp.model.PorBazdid;
import com.example.digikalaapp.model.PorForosh;
import com.example.digikalaapp.model.ProductDetails;
import com.example.digikalaapp.model.SignUp;
import com.example.digikalaapp.model.Slider;
import com.example.digikalaapp.model.UserComments;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerApi {
    @GET("image")
    Observable<List<Slider>> getSlider(@Query("id") int id);
    @GET("image")
    Observable<List<OfferOne>> getOfferItem(@Query("id") int id);
    @GET("product/{name}")
    Observable<List<Category>> showCategory(@Path("name") String name);
    @GET("image")
    Call<List<PorForosh>> getItemPF(@Query("id") int id);
    @GET("image")
    Observable<List<Category>> getBrand(@Query("id") int id);
    @GET("product/{name}")
    Call<List<PorBazdid>> showViews(@Path("name") String name);
    @GET("image")
    Observable<List<CategoryTwo>> getCategory(@Query("id") int id);
    @GET("image")
    Observable<List<PopularSearch>> getSearchBrand(@Query("id") int id);
    @GET("product/{name}")
    Observable<List<Details>> getDetail(@Path("name") String name);
    @GET("details/{name}")
    Observable<List<ProductDetails>> showProductDetails(@Path("name") String name);
    @GET("comments/{name}")
    Observable<List<UserComments>> showProductComments(@Path("name") String name);
    @FormUrlEncoded
    @POST("user/signup")
    Observable<SignUp> signUp(@Field("number") String number);
    @GET("introduce/{name}")
    Observable<List<Introduce>> introduceProduct(@Path("name") String name);
}
