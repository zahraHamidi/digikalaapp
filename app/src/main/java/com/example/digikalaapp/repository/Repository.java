package com.example.digikalaapp.repository;

import android.content.Context;
import android.content.SharedPreferences;

public class Repository {
    public static void SetShared(Context context , String number){
        SharedPreferences preferences = context.getSharedPreferences("userinfo",0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("number",number);
        editor.commit();
    }
    public static String GetShared(Context context){
        SharedPreferences preferences = context.getSharedPreferences("userinfo",0);
        return preferences.getString("number","not");
    }
}
