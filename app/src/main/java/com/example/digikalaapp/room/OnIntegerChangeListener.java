package com.example.digikalaapp.room;

public interface OnIntegerChangeListener {
    public void onIntegerChanged(int newValue);
}
