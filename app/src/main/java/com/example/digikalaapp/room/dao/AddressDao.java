package com.example.digikalaapp.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

import com.example.digikalaapp.room.model.ShoppingAddress;
import com.google.android.material.circularreveal.CircularRevealHelper;

@Dao
public interface AddressDao {
    @Insert
    public void insertAddress(ShoppingAddress address);
}
