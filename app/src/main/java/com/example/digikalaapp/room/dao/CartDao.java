package com.example.digikalaapp.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.digikalaapp.room.model.Cart;

import java.util.List;

@Dao
public interface CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCart(Cart cart);
    @Delete
    public void deleteCart(Cart cart);

    @Query("select * from Cart_Info")
    public List<Cart> getCartItems();

//    @Query("select * from Cart_Info where nameProduct = :name")
//    public void getInfo(String name);

    @Query("update Cart_Info set count = :Count where id = :pID")
    public int setCount(int Count , int pID);



}
