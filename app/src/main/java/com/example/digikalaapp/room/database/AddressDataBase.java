package com.example.digikalaapp.room.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.digikalaapp.room.dao.AddressDao;
import com.example.digikalaapp.room.dao.CartDao;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.room.model.ShoppingAddress;

@Database(entities = {ShoppingAddress.class},version = 2,exportSchema = false)
public abstract class AddressDataBase extends RoomDatabase {
    public static AddressDataBase database;
    public static String DATABASE_NAME="database";
    public synchronized static AddressDataBase getRoomInstance(Context context){
        if (database == null){
            database = Room.databaseBuilder(context.getApplicationContext(),
                    AddressDataBase.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
    public abstract AddressDao addressDao();
    //public abstract AddressDao addressDao();
}
