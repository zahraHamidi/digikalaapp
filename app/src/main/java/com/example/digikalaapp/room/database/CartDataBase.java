package com.example.digikalaapp.room.database;

import android.content.Context;
import android.os.Build;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.digikalaapp.room.dao.AddressDao;
import com.example.digikalaapp.room.dao.CartDao;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.room.model.ShoppingAddress;

import java.util.Arrays;

@Database(entities = {Cart.class},version = 1,exportSchema = false)
public abstract class CartDataBase extends RoomDatabase {
    public static CartDataBase database;
    public static String DATABASE_NAME="database";
    public synchronized static CartDataBase getRoomInstance(Context context){
        if (database == null){
            database = Room.databaseBuilder(context.getApplicationContext(),
                    CartDataBase.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
    public abstract CartDao cartDao();
    //public abstract AddressDao addressDao();
}
