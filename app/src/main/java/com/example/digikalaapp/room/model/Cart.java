package com.example.digikalaapp.room.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.bumptech.glide.Glide;

@Entity(tableName = "Cart_Info")
public class Cart {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "userId")
    public String userId;
    @ColumnInfo(name = "nameProduct")
    public String nameProduct;
    @ColumnInfo(name = "price")
    public int price;
    @ColumnInfo(name = "count")
    public int count;

    @ColumnInfo(name = "imageProduct")
    public String imageProduct;

    @BindingAdapter("android:LoadImage")
    public static void getImage(ImageView view , String url){
        Glide.with(view.getContext()).load(url).into(view);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }



    public String getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(String imageProduct) {
        this.imageProduct = imageProduct;
    }
}
