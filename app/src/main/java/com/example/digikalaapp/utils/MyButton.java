package com.example.digikalaapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.digikalaapp.R;

public class MyButton extends androidx.appcompat.widget.AppCompatButton {
    public MyButton(@NonNull Context context) {
        super(context);
        init();
    }

    public MyButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),getContext().getResources().getString(R.string.fontiran)) ;
        setTypeface(typeface);
    }
}
