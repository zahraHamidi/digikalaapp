package com.example.digikalaapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ActivityDetailsBinding;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.room.database.CartDataBase;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.utils.StatusBar;

public class DetailsActivity extends AppCompatActivity {

    ActivityDetailsBinding binding;
    public static String num;
    CartDataBase dataBase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_details);
        StatusBar.ChangeColor(this,R.color.gray);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_details);
        Intent intent = getIntent();
         num =intent.getStringExtra("name");
        String price =intent.getStringExtra("price");
        String preprice =intent.getStringExtra("preprice");
        String percent =intent.getStringExtra("percent");
        String image = intent.getStringExtra("image");
        binding.txtOrgPrc.setText(price);
        binding.txtPrc.setText(percent);



        SpannableString spannableString = new SpannableString(preprice);
        spannableString.setSpan(new StrikethroughSpan(),0,preprice.length(), Spanned.SPAN_MARK_MARK);
        binding.txtPrep.setText(spannableString);

        binding.btnShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id =Repository.GetShared(getApplicationContext());
                if (id != "not") {
                    dataBase = CartDataBase.getRoomInstance(getApplicationContext());
                    Cart cart = new Cart();
                    cart.setUserId(Repository.GetShared(getApplicationContext()));
                    cart.setCount(1);
                    cart.setPrice(Integer.valueOf(price));
                    cart.setNameProduct(num);
                    cart.setImageProduct(image);
                    dataBase.cartDao().insertCart(cart);
                    Toast.makeText(DetailsActivity.this, "این محصول به سبد خرید شما افزوده شد.", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(DetailsActivity.this, "ابتدا باید ثبت نام را انجام دهید", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }




}