package com.example.digikalaapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.digikalaapp.R;

public class FeatureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature);
    }
}