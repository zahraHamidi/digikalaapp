package com.example.digikalaapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.digikalaapp.R;
import com.example.digikalaapp.utils.StatusBar;

public class IntroduceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduce);
        StatusBar.ChangeColor(this,R.color.gray);
    }
}