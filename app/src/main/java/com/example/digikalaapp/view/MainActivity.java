package com.example.digikalaapp.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ActivityMainBinding;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.room.database.CartDataBase;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.utils.StatusBar;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    NavController navController;
    AppBarConfiguration appBarConfiguration;
   ActivityMainBinding binding;
   CartDataBase dataBase;
   List<Cart> cartList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        StatusBar.ChangeColor(this,R.color.gray);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        dataBase = CartDataBase.getRoomInstance(this);
        cartList.addAll(dataBase.cartDao().getCartItems());
        binding.bottomNavigation.setSelectedItemId(R.id.home);
        navController = Navigation.findNavController(this,R.id.fragment);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupWithNavController(binding.bottomNavigation,navController);

//        navController.navigate(R.id.homeFragment);

        BadgeDrawable badge_shop = binding.bottomNavigation.getOrCreateBadge(R.id.shopFragment);
        badge_shop.setBackgroundColor(Color.RED);
        badge_shop.setBadgeTextColor(Color.WHITE);
        badge_shop.setMaxCharacterCount(3);
        if (cartList.size() > 0){
            badge_shop.setNumber(cartList.size());
        }
        badge_shop.setVisible(true);

        binding.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.homeFragment: {
                        navController.navigate(R.id.homeFragment);
                       // binding.cardShow.setVisibility(View.VISIBLE);
                        if (binding.cardShow.getVisibility()== View.GONE){
                            binding.cardShow.setVisibility(View.VISIBLE);
                        }

                        break;
                    }

                    case R.id.categoryFragment:{
                        navController.navigate(R.id.categoryFragment);
                        binding.cardShow.setVisibility(View.VISIBLE);

                        break;
                    }
                    case R.id.shopFragment:{
                        navController.navigate(R.id.shopFragment);
                        binding.cardShow.setVisibility(View.GONE);

                        if (cartList.size() == 0){
                            BadgeDrawable badge_shop = binding.bottomNavigation.getBadge(R.id.shopFragment);
                            badge_shop.clearNumber();
                            badge_shop.setVisible(false);
                        }

                        break;
                    }
                    case R.id.profileFragment:{
                        String num = Repository.GetShared(MainActivity.this);
                        if (num != "not"){
                            navController.navigate(R.id.after_SignUp_Fragment);
                        }else {
                            navController.navigate(R.id.profileFragment);
                        }
                        binding.cardShow.setVisibility(View.GONE);

                        break;
                    }

                }
                return false;
            }
        });

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        finish();
//        startActivity(getIntent());
//    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController,appBarConfiguration);
    }


}