package com.example.digikalaapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ActivityPaymentBinding;
import com.example.digikalaapp.utils.StatusBar;
import com.example.digikalaapp.view.fragments.AddCartFragment;

public class PaymentActivity extends AppCompatActivity {

    ActivityPaymentBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_payment);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_payment);
        StatusBar.ChangeColor(this,R.color.redSplash);

        int totalPrice = AddCartFragment.priceproduct;
        binding.totalPrice.setText("مبلغ قابل پرداخت:"+totalPrice);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PaymentActivity.this,MainActivity.class));


    }
}