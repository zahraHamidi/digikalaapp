package com.example.digikalaapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.utils.StatusBar;

public class SplashActivity extends AppCompatActivity {

    ImageView digi_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        StatusBar.ChangeColor(this,R.color.fullred);
        digi_img = findViewById(R.id.img_digi_splash);
        SpringanimationX(digi_img,400f);
        SpringanimationX(digi_img,0f);
        networkInfo();
    }
    public void SpringanimationX(View view, float position){
        SpringAnimation springAnimation=new SpringAnimation(view, DynamicAnimation.TRANSLATION_X);
        SpringForce springForce=new SpringForce();
        springForce.setStiffness(SpringForce.STIFFNESS_LOW);
        springForce.setFinalPosition(position);
        springForce.setDampingRatio(SpringForce.DAMPING_RATIO_HIGH_BOUNCY);
        springAnimation.setSpring(springForce);
        springAnimation.start();
    }
    private void networkInfo(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connectivityManager.getActiveNetworkInfo();
        if (info != null){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    finish();
                }
            },2000);
        }
        else {
            Handler txthandler=new Handler();
            txthandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(SplashActivity.this, "اتصال شما برقرار نمی باشد", Toast.LENGTH_SHORT).show();

                }
            },1500);

        }
    }
}