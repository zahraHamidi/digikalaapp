package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemPorbazdidBinding;
import com.example.digikalaapp.model.PorBazdid;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class BazdidAdapter extends RecyclerView.Adapter<BazdidAdapter.BazdidViewHolder> {

    Context context;
    List<PorBazdid> bazdidList;

    public BazdidAdapter(Context context, List<PorBazdid> bazdidList) {
        this.context = context;
        this.bazdidList = bazdidList;
    }

    @NonNull
    @Override
    public BazdidViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemPorbazdidBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_porbazdid,parent,false);
        return new BazdidViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BazdidViewHolder holder, int position) {
            holder.binding.setViews(bazdidList.get(position));
            holder.binding.txtCount.setText(bazdidList.get(position).getFid()+"");
            holder.binding.itemprod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra("name",bazdidList.get(position).getDesc());
                    intent.putExtra("price",bazdidList.get(position).getPrice()+"");
                    intent.putExtra("preprice",bazdidList.get(position).getPre_price()+"");
                    intent.putExtra("percent",bazdidList.get(position).getPercent());
                    intent.putExtra("image",bazdidList.get(position).getImage());
                    context.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return bazdidList.size();
    }

    public class BazdidViewHolder extends RecyclerView.ViewHolder {
        ItemPorbazdidBinding binding;
        public BazdidViewHolder(@NonNull ItemPorbazdidBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
