package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemBrandBinding;
import com.example.digikalaapp.databinding.ItemMobileBinding;
import com.example.digikalaapp.model.Category;

import java.util.List;

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.BrandViewHolder> {

    Context context;
    List<Category> categoryList;

    public BrandAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public BrandViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBrandBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_brand,parent,false);
        return new BrandViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandViewHolder holder, int position) {

        holder.binding.setBrandInfo(categoryList.get(position));
        if (position == categoryList.size() - 1){
            holder.binding.brandview.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class BrandViewHolder extends RecyclerView.ViewHolder {
        ItemBrandBinding binding;
        public BrandViewHolder(@NonNull ItemBrandBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
