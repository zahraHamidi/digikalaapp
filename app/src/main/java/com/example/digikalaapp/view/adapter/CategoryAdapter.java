package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemMobileBinding;
import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MobileViewHolder> {

    Context context;
    List<Category> categoryList;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public MobileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemMobileBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_mobile,parent,false);
        return new MobileViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MobileViewHolder holder, int position) {

        holder.binding.setMobileInfo(categoryList.get(position));
        holder.binding.cardMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("name",categoryList.get(position).getDesc());
                intent.putExtra("price",categoryList.get(position).getPrice()+"");
                intent.putExtra("preprice",categoryList.get(position).getPre_price()+"");
                intent.putExtra("percent",categoryList.get(position).getPercent());
                intent.putExtra("image",categoryList.get(position).getImage());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class MobileViewHolder extends RecyclerView.ViewHolder {
        ItemMobileBinding binding;
        public MobileViewHolder(@NonNull ItemMobileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
