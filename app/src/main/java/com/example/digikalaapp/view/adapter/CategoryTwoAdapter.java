package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemCategoryBinding;
import com.example.digikalaapp.databinding.ItemMobileBinding;
import com.example.digikalaapp.model.CategoryTwo;

import java.util.List;

public class CategoryTwoAdapter extends RecyclerView.Adapter<CategoryTwoAdapter.CategoryViewHolder> {

    Context context;
    List<CategoryTwo> categoryList;

    public CategoryTwoAdapter(Context context, List<CategoryTwo> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemCategoryBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_category,parent,false);
        return new CategoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
            holder.binding.setCategoryInfo(categoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ItemCategoryBinding binding;
        public CategoryViewHolder(@NonNull ItemCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
