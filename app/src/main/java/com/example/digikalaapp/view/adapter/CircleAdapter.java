package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemCircleBinding;
import com.example.digikalaapp.model.CircleItem;

import java.util.List;

public class CircleAdapter extends RecyclerView.Adapter<CircleAdapter.CircleViewHolder> {

    Context context;
    List<CircleItem> circleItems;

    public CircleAdapter(Context context, List<CircleItem> circleItems) {
        this.context = context;
        this.circleItems = circleItems;
    }

    @NonNull
    @Override
    public CircleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_circle,parent,false);
       return new CircleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CircleViewHolder holder, int position) {
        holder.txt_cicle.setText(circleItems.get(position).getName());
        holder.img_circle.setImageResource(circleItems.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return circleItems.size();
    }

    public class CircleViewHolder extends RecyclerView.ViewHolder {
        TextView txt_cicle;
        ImageView img_circle;
        public CircleViewHolder(@NonNull View itemview) {
            super(itemview);
            txt_cicle = itemview.findViewById(R.id.txt_circle);
            img_circle = itemview.findViewById(R.id.img_circle);

        }
    }
}
