package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FeaturesLayoutBinding;
import com.example.digikalaapp.model.Features;
import com.example.digikalaapp.model.ProductDetails;

import java.util.List;

public class FeaturesAdapter extends RecyclerView.Adapter<FeaturesAdapter.FeaturesViewHolder> {

    Context context;
    List<Features> productDetails;

    public FeaturesAdapter(Context context, List<Features> productDetails) {
        this.context = context;
        this.productDetails = productDetails;
    }

    @NonNull
    @Override
    public FeaturesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        FeaturesLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.features_layout,parent,false);
        return new FeaturesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FeaturesViewHolder holder, int position) {
        holder.binding.setFeatures(productDetails.get(position));
        if (position == productDetails.size() - 1){
            holder.binding.featuresView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return productDetails.size();
    }

    public class FeaturesViewHolder extends RecyclerView.ViewHolder {
        FeaturesLayoutBinding binding;
        public FeaturesViewHolder(@NonNull FeaturesLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
