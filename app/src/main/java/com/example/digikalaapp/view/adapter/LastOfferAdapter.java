package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemLastOfferBinding;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class LastOfferAdapter extends RecyclerView.Adapter<LastOfferAdapter.LastOfferViewHolder> {

    Context context;
    List<OfferOne> lastOfferList;

    public LastOfferAdapter(Context context, List<OfferOne> lastOfferList) {
        this.context = context;
        this.lastOfferList = lastOfferList;
    }

    @NonNull
    @Override
    public LastOfferViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemLastOfferBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_last_offer,parent,false);
        return new LastOfferViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LastOfferViewHolder holder, int position) {
        String preprice=String.valueOf(lastOfferList.get(position).getPrice());
        SpannableString spannableString=new SpannableString(preprice);
        spannableString.setSpan(new StrikethroughSpan(),0,preprice.length(), Spanned.SPAN_MARK_MARK);
            holder.binding.setLastOfferInfo(lastOfferList.get(position));
            holder.binding.txtPreprice.setText(lastOfferList.get(position).getPre_price()+"");
            holder.binding.txtPrice.setText(spannableString);

        holder.binding.lastItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("name",lastOfferList.get(position).getDesc());
                intent.putExtra("price",lastOfferList.get(position).getPrice()+"");
                intent.putExtra("preprice",lastOfferList.get(position).getPre_price()+"");
                intent.putExtra("percent",lastOfferList.get(position).getPercent());
                intent.putExtra("image",lastOfferList.get(position).getImage());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lastOfferList.size();
    }

    public class LastOfferViewHolder extends RecyclerView.ViewHolder {
        ItemLastOfferBinding binding;
        public LastOfferViewHolder(@NonNull ItemLastOfferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
