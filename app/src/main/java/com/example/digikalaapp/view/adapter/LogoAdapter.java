package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemBrandTwoBinding;
import com.example.digikalaapp.model.Category;

import java.util.List;

public class LogoAdapter extends RecyclerView.Adapter<LogoAdapter.LogoViewHolder> {

    Context context;
    List<Category> logoList;

    public LogoAdapter(Context context, List<Category> logoList) {
        this.context = context;
        this.logoList = logoList;
    }

    @NonNull
    @Override
    public LogoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBrandTwoBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_brand_two,parent,false);
        return new LogoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LogoViewHolder holder, int position) {
        holder.binding.setLogoInfo(logoList.get(position));
    }

    @Override
    public int getItemCount() {
        return logoList.size();
    }

    public class LogoViewHolder extends RecyclerView.ViewHolder {
        ItemBrandTwoBinding binding;
        public LogoViewHolder(@NonNull ItemBrandTwoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
