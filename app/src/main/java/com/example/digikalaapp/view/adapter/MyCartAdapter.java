package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemBasketBinding;
import com.example.digikalaapp.databinding.ItemPorbazdidBinding;
import com.example.digikalaapp.room.database.CartDataBase;
import com.example.digikalaapp.room.model.Cart;

import java.util.List;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.MyCartViewHolder> {

    Context context;
   List<Cart> cartList ;
   CartDataBase dataBase;
   Listener listener;
    public MyCartAdapter(Context context, List<Cart> cartList,Listener listener) {
        this.context = context;
        this.cartList = cartList;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBasketBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_basket,parent,false);
        return new MyCartViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCartViewHolder holder, int position) {
        dataBase = CartDataBase.getRoomInstance(context);
        holder.binding.cartCount.setText(cartList.get(position).getCount()+"");
        holder.binding.cartPrice.setText(cartList.get(position).getPrice()+"");
        holder.binding.setCartInfo(cartList.get(position));
        holder.binding.deleteCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deletePrice(position);
                Cart cart = cartList.get(holder.getAdapterPosition());
                dataBase.cartDao().deleteCart(cart);

                int position = holder.getAdapterPosition();
                cartList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,cartList.size());

            }
        });

        holder.binding.addCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart c = cartList.get(holder.getAdapterPosition());
                int productId = c.getId();
                int count = c.getCount();
                count++;
                dataBase.cartDao().setCount(count,productId);
//                holder.binding.cartCount.setText(cartList.get(holder.getAdapterPosition()).getCount());
                listener.TotalPrice(cartList.get(position).getCount(),cartList.get(position).getPrice(),position);
                cartList.clear();
                cartList.addAll(dataBase.cartDao().getCartItems());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class MyCartViewHolder extends RecyclerView.ViewHolder {
        ItemBasketBinding binding;
        public MyCartViewHolder(@NonNull ItemBasketBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }

   public interface Listener{
        public void TotalPrice(int count, int price, int position);
        public void deletePrice(int position);
    }
}
