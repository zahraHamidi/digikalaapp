package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemOfferOneBinding;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferOneViewHolder> {

    Context context;
    List<OfferOne> offerOneList;

    public OfferAdapter(Context context, List<OfferOne> offerOneList) {
        this.context = context;
        this.offerOneList = offerOneList;
    }

    @NonNull
    @Override
    public OfferOneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemOfferOneBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_offer_one,parent,false);
        return new OfferOneViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferOneViewHolder holder, int position) {


            String preprice = String.valueOf(offerOneList.get(position).getPre_price());
            SpannableString spannableString = new SpannableString(preprice);
            spannableString.setSpan(new StrikethroughSpan(),0,preprice.length(), Spanned.SPAN_MARK_MARK);
            holder.binding.setOfferOneInfo(offerOneList.get(position));
            holder.binding.txtPrice.setText(offerOneList.get(position).getPrice()+"");
            holder.binding.txtPreprice.setText(spannableString);

            holder.binding.cardshow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra("name",offerOneList.get(position).getDesc());
                    intent.putExtra("price",offerOneList.get(position).getPrice()+"");
                    intent.putExtra("preprice",offerOneList.get(position).getPre_price()+"");
                    intent.putExtra("percent",offerOneList.get(position).getPercent());
                    intent.putExtra("image",offerOneList.get(position).getImage());
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return offerOneList.size();
    }

    public class OfferOneViewHolder extends RecyclerView.ViewHolder {
        ItemOfferOneBinding binding;
        public OfferOneViewHolder(@NonNull ItemOfferOneBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
