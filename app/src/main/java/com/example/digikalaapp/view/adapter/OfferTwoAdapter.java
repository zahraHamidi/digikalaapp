package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemOfferOneBinding;
import com.example.digikalaapp.databinding.ItemOfferTwoBinding;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class OfferTwoAdapter extends RecyclerView.Adapter<OfferTwoAdapter.OfferTwoViewHolder> {

    Context context;
    List<OfferOne> offerTwoList;

    public OfferTwoAdapter(Context context, List<OfferOne> offerTwoList) {
        this.context = context;
        this.offerTwoList = offerTwoList;
    }

    @NonNull
    @Override
    public OfferTwoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemOfferTwoBinding binding = DataBindingUtil.inflate(inflater,R.layout.item_offer_two,parent,false);
        return new OfferTwoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferTwoViewHolder holder, int position) {

        if (position == 0){
            holder.binding.imgOfferTwo.setVisibility(View.VISIBLE);
            holder.binding.cardshow.setVisibility(View.GONE);
            holder.binding.cardshowall.setVisibility(View.GONE);
        } else if (position == offerTwoList.size() - 1){
            holder.binding.imgOfferTwo.setVisibility(View.GONE);
            holder.binding.cardshow.setVisibility(View.GONE);
            holder.binding.cardshowall.setVisibility(View.VISIBLE);
        } else {
            holder.binding.imgOfferTwo.setVisibility(View.GONE);
            holder.binding.cardshow.setVisibility(View.VISIBLE);
            holder.binding.cardshowall.setVisibility(View.GONE);
        }
        String preprice = String.valueOf(offerTwoList.get(position).getPrice());
        SpannableString spannableString = new SpannableString(preprice);
        spannableString.setSpan(new StrikethroughSpan(),0,preprice.length(), Spanned.SPAN_MARK_MARK);
        holder.binding.setOfferTwoInfo(offerTwoList.get(position));
        holder.binding.txtPrice.setText(offerTwoList.get(position).getPre_price()+"");
        holder.binding.txtPreprice.setText(spannableString);

        holder.binding.cardshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("name",offerTwoList.get(position).getDesc());
                intent.putExtra("price",offerTwoList.get(position).getPre_price()+"");
                intent.putExtra("preprice",offerTwoList.get(position).getPrice()+"");
                intent.putExtra("percent",offerTwoList.get(position).getPercent());
                intent.putExtra("image",offerTwoList.get(position).getImage());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return offerTwoList.size();
    }


    public class OfferTwoViewHolder extends RecyclerView.ViewHolder {
        ItemOfferTwoBinding binding;
        public OfferTwoViewHolder(@NonNull ItemOfferTwoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
