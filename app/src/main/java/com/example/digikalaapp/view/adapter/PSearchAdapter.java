package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemPbrandBinding;
import com.example.digikalaapp.model.PopularSearch;

import java.util.List;

public class PSearchAdapter extends RecyclerView.Adapter<PSearchAdapter.SearchViewHolder> {

    Context context;
    List<PopularSearch> searchList;
    Listener listener;

    public PSearchAdapter(Context context, List<PopularSearch> searchList, Listener listener) {
        this.context = context;
        this.searchList = searchList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemPbrandBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_pbrand,parent,false);
        return new SearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
                holder.binding.setSearchInfo(searchList.get(position));
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        ItemPbrandBinding binding;
        public SearchViewHolder(@NonNull ItemPbrandBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.brandCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemSelected(searchList.get(getAdapterPosition()).getTitle());
                }
            });
        }
    }

    public interface Listener{
        void onItemSelected(String name);
    }
}
