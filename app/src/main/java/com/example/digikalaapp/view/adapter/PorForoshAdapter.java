package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemPorforoshBinding;
import com.example.digikalaapp.model.PorForosh;
import com.example.digikalaapp.view.DetailsActivity;

import java.util.List;

public class PorForoshAdapter extends RecyclerView.Adapter<PorForoshAdapter.PorForoshViewHolder> {

    Context context;
    List<PorForosh> porForoshList;

    public PorForoshAdapter(Context context, List<PorForosh> porForoshList) {
        this.context = context;
        this.porForoshList = porForoshList;
    }

    @NonNull
    @Override
    public PorForoshViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemPorforoshBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_porforosh,parent,false);
        return new PorForoshViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PorForoshViewHolder holder, int position) {
        holder.binding.setPForoshInfo(porForoshList.get(position));
        holder.binding.txtCount.setText(porForoshList.get(position).getFid()+"");
//        holder.binding.liearItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, DetailsActivity.class);
//                intent.putExtra("name",porForoshList.get(position).getName());
//                intent.putExtra("price",porForoshList.get(position).getPrice());
//                intent.putExtra("preprice",porForoshList.get(position).getPre_price());
//                intent.putExtra("percent",porForoshList.get(position).getPercent());
//                intent.putExtra("image",porForoshList.get(position).getImage());
//                context.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return porForoshList.size();
    }

    public class PorForoshViewHolder extends RecyclerView.ViewHolder {
        ItemPorforoshBinding binding;
        public PorForoshViewHolder(@NonNull ItemPorforoshBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.liearItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra("name",porForoshList.get(getAdapterPosition()).getName());
                    intent.putExtra("price",porForoshList.get(getAdapterPosition()).getPrice());
                    intent.putExtra("preprice",porForoshList.get(getAdapterPosition()).getPre_price());
                    intent.putExtra("percent",porForoshList.get(getAdapterPosition()).getPercent());
                    intent.putExtra("image",porForoshList.get(getAdapterPosition()).getImage());
                    context.startActivity(intent);
                }
            });
        }
    }
}
