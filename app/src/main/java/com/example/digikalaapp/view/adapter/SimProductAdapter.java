package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.SimilarProductBinding;
import com.example.digikalaapp.model.SimProduct;

import java.util.List;

public class SimProductAdapter extends RecyclerView.Adapter<SimProductAdapter.SimProductViewHolder> {

    Context context;
    List<SimProduct> simProductList;

    public SimProductAdapter(Context context, List<SimProduct> simProductList) {
        this.context = context;
        this.simProductList = simProductList;
    }

    @NonNull
    @Override
    public SimProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SimilarProductBinding binding = DataBindingUtil.inflate(inflater, R.layout.similar_product,parent,false);
        return new SimProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SimProductViewHolder holder, int position) {
            holder.binding.setSimilarProduct(simProductList.get(position));

        String preprice = String.valueOf(simProductList.get(position).getPre_price());
        SpannableString spannableString = new SpannableString(preprice);
        spannableString.setSpan(new StrikethroughSpan(),0,preprice.length(), Spanned.SPAN_MARK_MARK);
        holder.binding.txtPreprice.setText(spannableString);
        if (position == simProductList.size()-1){
            holder.binding.viewSimPrd.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return simProductList.size();
    }

    class SimProductViewHolder extends RecyclerView.ViewHolder {
        SimilarProductBinding binding;
        public SimProductViewHolder(@NonNull SimilarProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
