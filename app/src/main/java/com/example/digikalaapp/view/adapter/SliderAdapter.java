package com.example.digikalaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.ItemBanerSliderBinding;
import com.example.digikalaapp.model.Slider;

import java.util.List;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {
    Context context;
    List<Slider> sliderList;

    public SliderAdapter(Context context, List<Slider> sliderList) {
        this.context = context;
        this.sliderList = sliderList;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemBanerSliderBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_baner_slider,parent,false);
        return new SliderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        holder.binding.setSliderInfo(sliderList.get(position));
    }

    @Override
    public int getItemCount() {
        return sliderList.size();
    }

    public class SliderViewHolder extends RecyclerView.ViewHolder {
        ItemBanerSliderBinding binding;
        public SliderViewHolder(@NonNull ItemBanerSliderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
