package com.example.digikalaapp.view.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentAddCartBinding;
import com.example.digikalaapp.room.ObservableInteger;
import com.example.digikalaapp.room.OnIntegerChangeListener;
import com.example.digikalaapp.room.database.CartDataBase;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.view.AddressActivity;
import com.example.digikalaapp.view.adapter.MyCartAdapter;

import java.util.ArrayList;
import java.util.List;


public class AddCartFragment extends Fragment implements MyCartAdapter.Listener {

    FragmentAddCartBinding binding;
    CartDataBase dataBase;
    List<Cart> cartItems = new ArrayList<>();
    boolean check = false;

   public static int priceproduct = 0;

    public AddCartFragment() {
        // Required empty public constructor

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            dataBase=CartDataBase.getRoomInstance(getActivity());
            binding = FragmentAddCartBinding.inflate(inflater,container,false);
            cartItems.addAll(dataBase.cartDao().getCartItems());
            binding.AddCartRv.setAdapter(new MyCartAdapter(getActivity(),dataBase.cartDao().getCartItems(),this));








            //Toast.makeText(getActivity(), ""+price, Toast.LENGTH_SHORT).show();
            binding.btnShopCountinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), AddressActivity.class);
                    startActivity(intent);
                }
            });

        }
        return binding.getRoot();
    }


    @Override
    public void TotalPrice(int count, int price, int position) {



            priceproduct += price;

        binding.priceProduct.setText(priceproduct+"");
        binding.sumProduct.setText(priceproduct+"");
        binding.txtPrep.setText(priceproduct+"");

    }

    @Override
    public void deletePrice(int position) {
        priceproduct -=cartItems.get(position).getPrice()*cartItems.get(position).getCount();
        binding.priceProduct.setText(priceproduct+"");
        binding.sumProduct.setText(priceproduct+"");
        binding.txtPrep.setText(priceproduct+"");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!check){
        for (int i = 0; i < cartItems.size(); i++) {

            priceproduct += cartItems.get(i).getCount()*cartItems.get(i).getPrice();


        }
        binding.priceProduct.setText(priceproduct+"");
        binding.sumProduct.setText(priceproduct+"");
        binding.txtPrep.setText(priceproduct+"");
        check = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        priceproduct = 0;
    }
}