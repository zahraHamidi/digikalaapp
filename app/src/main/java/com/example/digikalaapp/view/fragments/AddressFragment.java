package com.example.digikalaapp.view.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentAddressBinding;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.room.database.AddressDataBase;
import com.example.digikalaapp.room.model.ShoppingAddress;
import com.example.digikalaapp.view.PaymentActivity;


public class AddressFragment extends Fragment {

    FragmentAddressBinding binding;
    AddressDataBase dataBase;
    EditText ostan, city, address, pelak, postalCode;

    public AddressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null) {
            binding = FragmentAddressBinding.inflate(inflater, container, false);
            dataBase = AddressDataBase.getRoomInstance(getActivity());
            ostan = binding.edtOstan;
            city = binding.edtCity;
            address = binding.edtAddress;
            pelak = binding.edtPelak;
            postalCode = binding.edtPostalCode;
            String num = Repository.GetShared(getActivity());

            binding.addAdress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ostan.getText().toString().isEmpty() && city.getText().toString().isEmpty() && address.getText().toString().isEmpty() && pelak.getText().toString().isEmpty() && postalCode.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "پر کردن تمامی فیلد ها ضروری می باشد.", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        ShoppingAddress shoppingAddress = new ShoppingAddress();
                        shoppingAddress.setOstan(ostan.getText().toString());
                        shoppingAddress.setCity(city.getText().toString());
                        shoppingAddress.setAddress(address.getText().toString());
                        shoppingAddress.setPelak(pelak.getText().toString());
                        shoppingAddress.setPostalCode(postalCode.getText().toString());
                        shoppingAddress.setUserId(num);
                        dataBase.addressDao().insertAddress(shoppingAddress);
                        Toast.makeText(getActivity(), "آدرس شما ثبت گردید.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), PaymentActivity.class);
                        startActivity(intent);
                    }


                }
            });


        }
        return binding.getRoot();
    }
}