package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentAfterSignUpBinding;
import com.example.digikalaapp.model.Order;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.view.adapter.OrderAdapter;

import java.util.ArrayList;
import java.util.List;


public class After_SignUp_Fragment extends Fragment {

    FragmentAfterSignUpBinding binding;
    List<Order> orderList=new ArrayList<>();
    OrderAdapter adapter ;

    public After_SignUp_Fragment() {
        // Required empty public constructor
        set_Recycler();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentAfterSignUpBinding.inflate(inflater,container,false);
            adapter = new OrderAdapter(getActivity(),orderList);
            binding.recySg.setAdapter(adapter);
            String num = Repository.GetShared(getActivity());
            if (num !="not"){
                binding.txtNumber.setText(num);
            }else{
                binding.txtNumber.setText("");
            }
        }
        return binding.getRoot();
    }
    public void set_Recycler(){
        orderList.add(new Order(R.drawable.clock,"در انتظار پرداخت"));
        orderList.add(new Order(R.drawable.cloud,"در حال پردازش"));
        orderList.add(new Order(R.drawable.ok,"تحویل شده"));
        orderList.add(new Order(R.drawable.cancle,"لغو شده"));
        orderList.add(new Order(R.drawable.back,"مرجوع شده"));
    }
}