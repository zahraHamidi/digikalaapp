package com.example.digikalaapp.view.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentCategoryBinding;
import com.example.digikalaapp.databinding.FragmentHomeBinding;
import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.model.CategoryTwo;
import com.example.digikalaapp.model.PopularSearch;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;
import com.example.digikalaapp.view.adapter.CategoryTwoAdapter;
import com.example.digikalaapp.view.adapter.LogoAdapter;
import com.example.digikalaapp.view.adapter.PSearchAdapter;
import com.example.digikalaapp.viewmodel.CategoryBrandViewModel;
import com.example.digikalaapp.viewmodel.CategoryDigitalViewModel;
import com.example.digikalaapp.viewmodel.CategoryFoodViewModel;
import com.example.digikalaapp.viewmodel.CategoryModeViewModel;
import com.example.digikalaapp.viewmodel.CategorySalamatViewModel;

import java.util.List;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CategoryFragment extends Fragment implements PSearchAdapter.Listener {

    FragmentCategoryBinding binding;
    ProgressDialog progressDialog;
    Handler handler;

    public CategoryFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentCategoryBinding.inflate(inflater,container,false);
            progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress);
            progressDialog.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent
            );

            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            },3000);
            CategoryDigitalViewModel digitalViewModel = new ViewModelProvider(this).get(CategoryDigitalViewModel.class);
            digitalViewModel.getcategoryList().observe(requireActivity(), new Observer<List<CategoryTwo>>() {
                @Override
                public void onChanged(List<CategoryTwo> categoryTwos) {
                    binding.recyDigital.setAdapter(new CategoryTwoAdapter(requireActivity(),categoryTwos));
                }
            });
            digitalViewModel.getCategoryApi();

            CategoryModeViewModel modeViewModel = new ViewModelProvider(this).get(CategoryModeViewModel.class);
            modeViewModel.getcategoryList().observe(requireActivity(), new Observer<List<CategoryTwo>>() {
                @Override
                public void onChanged(List<CategoryTwo> categoryTwos) {
                    binding.recyMod.setAdapter(new CategoryTwoAdapter(requireActivity(),categoryTwos));
                }
            });
            modeViewModel.getCategoryApi();

            CategorySalamatViewModel salamatViewModel = new ViewModelProvider(this).get(CategorySalamatViewModel.class);
            salamatViewModel.getcategoryList().observe(requireActivity(), new Observer<List<CategoryTwo>>() {
                @Override
                public void onChanged(List<CategoryTwo> categoryTwos) {
                    binding.recySalamat.setAdapter(new CategoryTwoAdapter(requireActivity(),categoryTwos));
                }
            });
            salamatViewModel.getCategoryApi();

            CategoryFoodViewModel foodViewModel = new ViewModelProvider(this).get(CategoryFoodViewModel.class);
            foodViewModel.getcategoryList().observe(requireActivity(), new Observer<List<CategoryTwo>>() {
                @Override
                public void onChanged(List<CategoryTwo> categoryTwos) {
                    binding.recyKhordani.setAdapter(new CategoryTwoAdapter(requireActivity(),categoryTwos));
                }
            });
            foodViewModel.getCategoryApi();

            CategoryBrandViewModel brandViewModel = new ViewModelProvider(this).get(CategoryBrandViewModel.class);
            brandViewModel.getcategoryList().observe(requireActivity(), new Observer<List<PopularSearch>>() {
                @Override
                public void onChanged(List<PopularSearch> popularSearches) {
                    setRV_Brand(popularSearches);
                }
            });
            brandViewModel.getCategoryApi();

            ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
            Observable<List<Category>> logoObservable = serverApi.showCategory("کالای دیجیتال")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
            logoObservable.subscribe(new io.reactivex.rxjava3.core.Observer<List<Category>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull List<Category> categories) {
                    binding.recyBrandLogo.setAdapter(new LogoAdapter(requireActivity(),categories));
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Log.e("Error", "onError: "+e.toString() );
                }

                @Override
                public void onComplete() {

                }
            });
        }
        return binding.getRoot();
    }
    public void setRV_Brand(List<PopularSearch> searches){
        PSearchAdapter searchAdapter = new PSearchAdapter(requireActivity(),searches,this);
        binding.recyPbrand.setAdapter(searchAdapter);
    }

    @Override
    public void onItemSelected(String name) {

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<Category>> logoObservable = serverApi.showCategory(name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        logoObservable.subscribe(new io.reactivex.rxjava3.core.Observer<List<Category>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<Category> categories) {
                binding.recyBrandLogo.setAdapter(new LogoAdapter(requireActivity(),categories));
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });
    }
}