package com.example.digikalaapp.view.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentDetailsBinding;
import com.example.digikalaapp.model.Details;
import com.example.digikalaapp.model.UserComments;
import com.example.digikalaapp.view.DetailsActivity;
import com.example.digikalaapp.view.FeatureActivity;
import com.example.digikalaapp.view.IntroduceActivity;
import com.example.digikalaapp.view.adapter.CommentAdapter;
import com.example.digikalaapp.view.adapter.OfferOneAdapter;
import com.example.digikalaapp.view.adapter.SimProductAdapter;
import com.example.digikalaapp.viewmodel.CommentViewModel;
import com.example.digikalaapp.viewmodel.DetailOneViewModel;

import java.util.List;


public class DetailsFragment extends Fragment {


    FragmentDetailsBinding binding;

    public DetailsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding==null){
            binding = FragmentDetailsBinding.inflate(inflater,container,false);

            String nameProduct = DetailsActivity.num;

            DetailOneViewModel detailOneViewModel = new ViewModelProvider(this).get(DetailOneViewModel.class);
            detailOneViewModel.getDetailList().observe(requireActivity(), new Observer<List<Details>>() {
                @Override
                public void onChanged(List<Details> details) {
                    for (int i = 0; i <details.size() ; i++) {
                        Glide.with(requireActivity()).load(details.get(i).getImage())
                                .into(binding.imgPrdDetail);
                        binding.txtPrdName.setText(details.get(i).getName());
                        binding.similarPrd.setAdapter(new SimProductAdapter(requireActivity(),details.get(i).getImageList()));
                    }
                }
            });
            detailOneViewModel.detailsApi(nameProduct);

            CommentViewModel commentViewModel = new ViewModelProvider(this).get(CommentViewModel.class);
            commentViewModel.getCommentMutable().observe(requireActivity(), new Observer<List<UserComments>>() {
                @Override
                public void onChanged(List<UserComments> userComments) {
                    for (int i = 0; i <userComments.size() ; i++) {
                        binding.setNumberComment(userComments.get(i));
                        binding.commentRv.setAdapter(new CommentAdapter(requireActivity(),userComments.get(i).getComments()));
                    }
                }
            });
            commentViewModel.commentApi(nameProduct);

            binding.layoutDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), FeatureActivity.class);
                    startActivity(intent);
                }
            });
            binding.layoutIntroduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), IntroduceActivity.class);
                    startActivity(intent);
                }
            });




        }
        return binding.getRoot();
    }



}