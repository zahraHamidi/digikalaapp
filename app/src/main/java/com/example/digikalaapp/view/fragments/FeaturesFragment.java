package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentFeaturesBinding;
import com.example.digikalaapp.model.ProductDetails;
import com.example.digikalaapp.view.DetailsActivity;
import com.example.digikalaapp.view.adapter.FeaturesAdapter;
import com.example.digikalaapp.viewmodel.FeaturesViewModel;

import java.util.List;


public class FeaturesFragment extends Fragment {

    FragmentFeaturesBinding binding;

    public FeaturesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentFeaturesBinding.inflate(inflater,container,false);
            String nameProductDetail = DetailsActivity.num;
           // Toast.makeText(getActivity(), ""+nameProductDetail, Toast.LENGTH_SHORT).show();
            FeaturesViewModel featuresViewModel = new ViewModelProvider(this).get(FeaturesViewModel.class);
            featuresViewModel.getProductDetailsMutable().observe(requireActivity(), new Observer<List<ProductDetails>>() {
                @Override
                public void onChanged(List<ProductDetails> productDetails) {
                    for (int i = 0; i <productDetails.size() ; i++) {
                        if (productDetails.get(i).getTitleTwo() != null) {
                            binding.setProductDetail(productDetails.get(i));
                            binding.featurervTwo.setAdapter(new FeaturesAdapter(requireActivity(), productDetails.get(i).getDetailsTwo()));
                            binding.featurervThree.setAdapter(new FeaturesAdapter(requireActivity(), productDetails.get(i).getDetailsThree()));
                            binding.featurervFour.setAdapter(new FeaturesAdapter(requireActivity(), productDetails.get(i).getDetailsFour()));
                            binding.featurervFive.setAdapter(new FeaturesAdapter(requireActivity(), productDetails.get(i).getDetailsFive()));
                            binding.featurervSix.setAdapter(new FeaturesAdapter(requireActivity(), productDetails.get(i).getDetailsSix()));
                        }
                        binding.txtTitle.setText(productDetails.get(i).getTitle());
                        binding.featurerv.setAdapter(new FeaturesAdapter(requireActivity(),productDetails.get(i).getDetails()));

                    }

                }
            });
            featuresViewModel.FeaturesApi(nameProductDetail);
        }
        return binding.getRoot();
    }
}