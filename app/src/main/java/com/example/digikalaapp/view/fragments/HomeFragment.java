package com.example.digikalaapp.view.fragments;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentHomeBinding;
import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.model.CircleItem;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.model.PorBazdid;
import com.example.digikalaapp.model.PorForosh;
import com.example.digikalaapp.model.Slider;
import com.example.digikalaapp.view.adapter.BazdidAdapter;
import com.example.digikalaapp.view.adapter.BrandAdapter;
import com.example.digikalaapp.view.adapter.CategoryAdapter;
import com.example.digikalaapp.view.adapter.CircleAdapter;
import com.example.digikalaapp.view.adapter.LastOfferAdapter;
import com.example.digikalaapp.view.adapter.OfferOneAdapter;
import com.example.digikalaapp.view.adapter.OfferTwoAdapter;
import com.example.digikalaapp.view.adapter.PorForoshAdapter;
import com.example.digikalaapp.view.adapter.SliderAdapter;
import com.example.digikalaapp.viewmodel.AdverEightViewModel;
import com.example.digikalaapp.viewmodel.AdverFiveViewModel;
import com.example.digikalaapp.viewmodel.AdverFourViewModel;
import com.example.digikalaapp.viewmodel.AdverOneViewModel;
import com.example.digikalaapp.viewmodel.AdverSevenViewModel;
import com.example.digikalaapp.viewmodel.AdverSixViewModel;
import com.example.digikalaapp.viewmodel.AdverThreeViewModel;
import com.example.digikalaapp.viewmodel.AdverTwoViewModel;
import com.example.digikalaapp.viewmodel.BazdidViewModel;
import com.example.digikalaapp.viewmodel.BrandViewModel;
import com.example.digikalaapp.viewmodel.CategoryCoverViewModel;
import com.example.digikalaapp.viewmodel.CategoryHFViewModel;
import com.example.digikalaapp.viewmodel.CategoryMobileViewModel;
import com.example.digikalaapp.viewmodel.LastOfferViewModel;
import com.example.digikalaapp.viewmodel.OfferOneViewModel;
import com.example.digikalaapp.viewmodel.OfferTwoViewModel;
import com.example.digikalaapp.viewmodel.PorForoshViewModel;
import com.example.digikalaapp.viewmodel.SliderViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends Fragment {
    FragmentHomeBinding binding;
    public int position=0;
    boolean end=false;
    List<CircleItem> items = new ArrayList<>();
    ProgressDialog progressDialog;
    Handler handler;
    private class AutoScrollTask extends TimerTask {

        @Override
        public void run() {
            if (position == 5){
                end=true;
                position=0;
            }else if (position==0){
                end=false;
            }
            if (!end){
                position++;
            }
//            else {
//                position--;
//            }
            binding.sliderRecycle.smoothScrollToPosition(position);
        }
    }

    public HomeFragment() {
        // Required empty public constructor
        setCircleRecy();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentHomeBinding.inflate(inflater,container,false);
            CircleAdapter adapter = new CircleAdapter(requireActivity(),items);
            binding.circleRecycle.setAdapter(adapter);

            progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress);
            progressDialog.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent
            );

            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            },3000);

            SliderViewModel sliderViewModel = new ViewModelProvider(this).get(SliderViewModel.class);
            sliderViewModel.getSliders().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> sliders) {
                    binding.sliderRecycle.setAdapter(new SliderAdapter(requireActivity(),sliders));
                    binding.indicator.attachToRecyclerView(binding.sliderRecycle);
                    Timer timer = new Timer();
                    timer.scheduleAtFixedRate(new AutoScrollTask(),2000,5000);
                }
            });
            sliderViewModel.getSliderApi();

            OfferOneViewModel offerOneViewModel = new ViewModelProvider(this).get(OfferOneViewModel.class);
            offerOneViewModel.getOfferOneList().observe(requireActivity(), new Observer<List<OfferOne>>() {
                @Override
                public void onChanged(List<OfferOne> offerOnes) {
                    binding.recyOfferOne.setAdapter(new OfferOneAdapter(requireActivity(),offerOnes));
                }
            });
            offerOneViewModel.getOfferOneApi();

            AdverOneViewModel adverOneViewModel = new ViewModelProvider(this).get(AdverOneViewModel.class);
            adverOneViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver1);
                    }
                }
            });
            adverOneViewModel.AdverOneApi();
            AdverTwoViewModel adverTwoViewModel = new ViewModelProvider(this).get(AdverTwoViewModel.class);
            adverTwoViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver2);
                    }
                }
            });
            adverTwoViewModel.AdverTwoApi();
            AdverThreeViewModel adverThreeViewModel = new ViewModelProvider(this).get(AdverThreeViewModel.class);
            adverThreeViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver3);
                    }
                }
            });
            adverThreeViewModel.AdverThreeApi();

            AdverFourViewModel adverFourViewModel = new ViewModelProvider(this).get(AdverFourViewModel.class);
            adverFourViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver4);
                    }
                }
            });
            adverFourViewModel.AdverFourApi();

            OfferTwoViewModel OfferTwoViewModel = new ViewModelProvider(this).get(OfferTwoViewModel.class);
            OfferTwoViewModel.getOfferTwoList().observe(requireActivity(), new Observer<List<OfferOne>>() {
                @Override
                public void onChanged(List<OfferOne> offerTwo) {
                    binding.recyOfferTwo.setAdapter(new OfferTwoAdapter(requireActivity(),offerTwo));
                }
            });
            OfferTwoViewModel.getOfferTwoApi();

            CategoryMobileViewModel mobileViewModel = new ViewModelProvider(this).get(CategoryMobileViewModel.class);
            mobileViewModel.getcategoryList().observe(requireActivity(), new Observer<List<Category>>() {
                @Override
                public void onChanged(List<Category> categories) {
                    binding.recyMobile.setAdapter(new CategoryAdapter(requireActivity(),categories));
                }
            });
            mobileViewModel.getCategoryApi();

            AdverFiveViewModel adverFiveViewModel = new ViewModelProvider(this).get(AdverFiveViewModel.class);
            adverFiveViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver5);
                    }
                }
            });
            adverFiveViewModel.AdverFiveApi();
            CategoryCoverViewModel coverViewModel = new ViewModelProvider(this).get(CategoryCoverViewModel.class);
            coverViewModel.getcategoryList().observe(requireActivity(), new Observer<List<Category>>() {
                @Override
                public void onChanged(List<Category> categories) {
                    binding.recyCover.setAdapter(new CategoryAdapter(requireActivity(),categories));
                }
            });
            coverViewModel.getCategoryApi();

            AdverSixViewModel adverSixViewModel = new ViewModelProvider(this).get(AdverSixViewModel.class);
            adverSixViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver6);
                    }
                }
            });
            adverSixViewModel.AdverSixApi();

            PorForoshViewModel porForoshViewModel = new ViewModelProvider(this).get(PorForoshViewModel.class);
            porForoshViewModel.getPorForoshList().observe(requireActivity(), new Observer<List<PorForosh>>() {
                @Override
                public void onChanged(List<PorForosh> porForoshes) {
                    binding.recyPf.setAdapter(new PorForoshAdapter(requireActivity(),porForoshes));
                }
            });
            porForoshViewModel.porForoshApi();

            AdverSevenViewModel adverSevenViewModel = new ViewModelProvider(this).get(AdverSevenViewModel.class);
            adverSevenViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver7);
                    }
                }
            });
            adverSevenViewModel.AdverSevenApi();

            CategoryHFViewModel categoryHFViewModel = new ViewModelProvider(this).get(CategoryHFViewModel.class);
            categoryHFViewModel.getcategoryList().observe(requireActivity(), new Observer<List<Category>>() {
                @Override
                public void onChanged(List<Category> categories) {
                    binding.recyHf.setAdapter(new CategoryAdapter(requireActivity(),categories));
                }
            });
            categoryHFViewModel.getCategoryApi();

            AdverEightViewModel adverEightViewModel = new ViewModelProvider(this).get(AdverEightViewModel.class);
            adverEightViewModel.getAdverList().observe(requireActivity(), new Observer<List<Slider>>() {
                @Override
                public void onChanged(List<Slider> advers) {
                    for (int i = 0; i < advers.size(); i++) {
                        Glide.with(requireActivity()).load(advers.get(i).getImage())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .into(binding.imgAdver8);
                    }
                }
            });
            adverEightViewModel.AdverEightApi();

            BrandViewModel brandViewModel = new ViewModelProvider(this).get(BrandViewModel.class);
            brandViewModel.getbrandList().observe(requireActivity(), new Observer<List<Category>>() {
                @Override
                public void onChanged(List<Category> brands) {
                    binding.recyBrand.setAdapter(new BrandAdapter(requireActivity(),brands));
                }
            });
            brandViewModel.getBrandApi();

            BazdidViewModel bazdidViewModel = new ViewModelProvider(this).get(BazdidViewModel.class);
            bazdidViewModel.getBazdidList().observe(requireActivity(), new Observer<List<PorBazdid>>() {
                @Override
                public void onChanged(List<PorBazdid> porBazdids) {
                    binding.recyViews.setAdapter(new BazdidAdapter(requireActivity(),porBazdids));
                }
            });
            bazdidViewModel.getbazdidApi();

            LastOfferViewModel lastOfferViewModel = new ViewModelProvider(this).get(LastOfferViewModel.class);
            lastOfferViewModel.getLastOfferList().observe(requireActivity(), new Observer<List<OfferOne>>() {
                @Override
                public void onChanged(List<OfferOne> lastOffers) {
                    binding.recyGrid.setAdapter(new LastOfferAdapter(requireActivity(),lastOffers));
                }
            });
            lastOfferViewModel.getLastOfferApi();

            requireActivity().getOnBackPressedDispatcher().addCallback(getActivity(), new OnBackPressedCallback(true) {
                @Override
                public void handleOnBackPressed() {
                    getActivity().finishAffinity();
                }
            });

        }
        // View view = inflater.inflate(R.layout.fragment_home, container, false);


        return binding.getRoot();
    }



    public void setCircleRecy(){
        items.add(new CircleItem(R.drawable.add_user,"خرید اولی ها"));
        items.add(new CircleItem(R.drawable.med,"برنده شو"));
        items.add(new CircleItem(R.drawable.apple,"کالای دیجیتال"));
        items.add(new CircleItem(R.drawable.commerce,"سوپر مارکت"));
        items.add(new CircleItem(R.drawable.cloth,"مد و پوشاک"));
    }
}