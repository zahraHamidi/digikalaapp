package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentIntroduceBinding;
import com.example.digikalaapp.model.Introduce;
import com.example.digikalaapp.view.DetailsActivity;
import com.example.digikalaapp.viewmodel.IntroduceProductViewModel;

import java.util.List;


public class IntroduceFragment extends Fragment {


    FragmentIntroduceBinding binding;
    public IntroduceFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentIntroduceBinding.inflate(inflater,container,false);
            String nameProduct = DetailsActivity.num;
            IntroduceProductViewModel introduceProductViewModel = new ViewModelProvider(this).get(IntroduceProductViewModel.class);
            introduceProductViewModel.getIntroduceList().observe(requireActivity(), new Observer<List<Introduce>>() {
                @Override
                public void onChanged(List<Introduce> introduces) {
                    for (int i=0 ; i < introduces.size() ; i++) {
                        binding.setIntroduceInfo(introduces.get(i));
                    }
                }
            });
            introduceProductViewModel.introduceApi(nameProduct);
        }
        return binding.getRoot();
    }
}