package com.example.digikalaapp.view.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentNextShopBinding;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.view.MainActivity;


public class Next_Shop_Fragment extends Fragment {


    FragmentNextShopBinding binding;
    NavController navController;
    CardView cardSignUp;
    public Next_Shop_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentNextShopBinding.inflate(inflater,container,false);
            cardSignUp = binding.cardSignUp;
            navController = Navigation.findNavController(requireActivity(),R.id.fragment);
            setHasOptionsMenu(true);
            cardSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String num = Repository.GetShared(requireActivity());
                    if (num != "not"){
                        navController.navigate(R.id.after_SignUp_Fragment);
                    }else {
                        navController.navigate(R.id.profileFragment);
                    }
                }
            });
//            requireActivity().getOnBackPressedDispatcher().addCallback(getActivity(), new OnBackPressedCallback(true) {
//                @SuppressLint("ResourceType")
//                @Override
//                public void handleOnBackPressed() {
//                    navController.navigate(R.id.action_next_Shop_Fragment_to_homeFragment);
//
//                }
//            });




        }
        return binding.getRoot();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item,navController)|| super.onOptionsItemSelected(item);
    }

}