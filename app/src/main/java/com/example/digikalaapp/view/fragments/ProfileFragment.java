package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentProfileBinding;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;
import com.example.digikalaapp.repository.Repository;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;


public class ProfileFragment extends Fragment {

    FragmentProfileBinding binding;
    NavController navController;

    public ProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentProfileBinding.inflate(inflater,container,false);
            navController = Navigation.findNavController(getActivity(),R.id.fragment);
            setHasOptionsMenu(true);
        }

        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = binding.edtSignUp.getText().toString();
                if (number.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")){
                    Toast.makeText(getActivity(), "لطفا شماره همراه خود را وارد نمایید.", Toast.LENGTH_SHORT).show();
                }
                else if (!number.matches("(\\+98|0)?9\\d{9}")){
                    Toast.makeText(getActivity(), "شماره همراه وارد شده معتبر نمی باشد.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Repository.SetShared(getActivity(),number);
                    ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
                    serverApi.signUp(number)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe();

                    navController.navigate(R.id.action_profileFragment_to_after_SignUp_Fragment);
                }
            }
        });

//        requireActivity().getOnBackPressedDispatcher().addCallback(getActivity(), new OnBackPressedCallback(true) {
//            @Override
//            public void handleOnBackPressed() {
//
//                    navController.navigate(R.id.homeFragment);
//
//            }
//        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item,navController)|| super.onOptionsItemSelected(item);
    }
}