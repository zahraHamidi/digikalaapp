package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.digikalaapp.R;
import com.example.digikalaapp.databinding.FragmentShopBasketBinding;
import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.repository.Repository;
import com.example.digikalaapp.view.MainActivity;
import com.example.digikalaapp.view.adapter.OfferAdapter;
import com.example.digikalaapp.view.adapter.OfferOneAdapter;
import com.example.digikalaapp.viewmodel.OfferOneViewModel;

import java.util.List;


public class ShopBasketFragment extends Fragment {

    FragmentShopBasketBinding binding;
    NavController navController;
    CardView cardSignUp;

    public ShopBasketFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (binding == null){
            binding = FragmentShopBasketBinding.inflate(inflater,container,false);
            cardSignUp = binding.cardSignUp;
            OfferOneViewModel offerViewModel = new ViewModelProvider(this).get(OfferOneViewModel.class);
            offerViewModel.getOfferOneList().observe(requireActivity(), new Observer<List<OfferOne>>() {
                @Override
                public void onChanged(List<OfferOne> offers) {
                    binding.recyOffer.setAdapter(new OfferAdapter(requireActivity(),offers));
                }
            });
            offerViewModel.getOfferOneApi();

            navController = Navigation.findNavController(requireActivity(),R.id.fragment);
            setHasOptionsMenu(true);
            cardSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String num = Repository.GetShared(requireActivity());
                    if (num != "not"){
                        navController.navigate(R.id.after_SignUp_Fragment);
                    }else {
                        navController.navigate(R.id.profileFragment);
                    }
                }
            });
//            requireActivity().getOnBackPressedDispatcher().addCallback(getActivity(), new OnBackPressedCallback(true) {
//                @Override
//                public void handleOnBackPressed() {
//                    navController.navigate(R.id.homeFragment);
//                }
//            });


        }
        return binding.getRoot();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item,navController)|| super.onOptionsItemSelected(item);
    }
}