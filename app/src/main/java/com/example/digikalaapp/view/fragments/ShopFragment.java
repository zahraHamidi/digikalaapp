package com.example.digikalaapp.view.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.digikalaapp.R;
import com.example.digikalaapp.room.database.CartDataBase;
import com.example.digikalaapp.room.model.Cart;
import com.example.digikalaapp.view.adapter.MyCartAdapter;
import com.example.digikalaapp.view.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class ShopFragment extends Fragment {


    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    List<Cart> cartItems =new ArrayList<>();
    CartDataBase dataBase;

    public ShopFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        dataBase = CartDataBase.getRoomInstance(getActivity());
        cartItems.addAll(dataBase.cartDao().getCartItems());
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupViewpager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewpager(ViewPager viewPager) {
        viewPagerAdapter=new ViewPagerAdapter(getChildFragmentManager());

        if (cartItems.size() > 0) {
            viewPagerAdapter.addFragment(new AddCartFragment(),"سبد خرید");
        } else if (cartItems.size() == 0){
            Toast.makeText(getActivity(), "سبد خرید خالی می باشد.", Toast.LENGTH_SHORT).show();
            viewPagerAdapter.addFragment(new ShopBasketFragment(),"سبد خرید");
        }

        viewPagerAdapter.addFragment(new Next_Shop_Fragment(),"لیست خرید بعدی");

        viewPager.setAdapter(viewPagerAdapter);
    }
}