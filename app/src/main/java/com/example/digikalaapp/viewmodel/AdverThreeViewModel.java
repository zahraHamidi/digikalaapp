package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.Slider;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AdverThreeViewModel extends ViewModel {

    public MutableLiveData<List<Slider>> adverList = new MutableLiveData<>();

    public MutableLiveData<List<Slider>> getAdverList(){
        return adverList;
    }

    public void AdverThreeApi(){
        ServerApi adverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<Slider>> adverObservable = adverApi.getSlider(23)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        adverObservable.subscribe(new Observer<List<Slider>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<Slider> advers) {
                    adverList.setValue(advers);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
