package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.PorBazdid;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BazdidViewModel extends ViewModel {

    public MutableLiveData<List<PorBazdid>> bazdidList = new MutableLiveData<>();

    public MutableLiveData<List<PorBazdid>> getBazdidList(){
        return bazdidList;
    }

    public void getbazdidApi(){
        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Call<List<PorBazdid>> call = serverApi.showViews("mobile");
        call.enqueue(new Callback<List<PorBazdid>>() {
            @Override
            public void onResponse(Call<List<PorBazdid>> call, Response<List<PorBazdid>> response) {
                List<PorBazdid> bazdids = new ArrayList<>();
                for (int i = 0; i <response.body().toArray().length ; i++) {
                    PorBazdid bazdid = new PorBazdid();
                    bazdid.setFid(i+1);
                    bazdid.setDesc(response.body().get(i).getDesc());
                    bazdid.setImage(response.body().get(i).getImage());
                    bazdid.setPrice(response.body().get(i).getPrice());
                    bazdid.setPre_price(response.body().get(i).getPre_price());
                    bazdid.setPercent(response.body().get(i).getPercent());
                    bazdids.add(bazdid);
                }
                bazdidList.setValue(bazdids);
            }

            @Override
            public void onFailure(Call<List<PorBazdid>> call, Throwable t) {
                Log.e("Error", "onError: "+t.toString() );

            }
        });
    }
}
