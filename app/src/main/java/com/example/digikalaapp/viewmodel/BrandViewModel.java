package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class BrandViewModel extends ViewModel {

    public MutableLiveData<List<Category>> brandlist=new MutableLiveData<>();

    public MutableLiveData<List<Category>> getbrandList(){
        return brandlist;
    }

    public void getBrandApi(){

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<Category>> brandObservable = serverApi.getBrand(7)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        brandObservable.subscribe(new Observer<List<Category>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<Category> brands) {
                brandlist.setValue(brands);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });


    }
}
