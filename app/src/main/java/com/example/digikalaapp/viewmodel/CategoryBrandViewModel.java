package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.CategoryTwo;
import com.example.digikalaapp.model.PopularSearch;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CategoryBrandViewModel extends ViewModel {

    public MutableLiveData<List<PopularSearch>> categorybrandlist=new MutableLiveData<>();

    public MutableLiveData<List<PopularSearch>> getcategoryList(){
        return categorybrandlist;
    }

    public void getCategoryApi(){

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<PopularSearch>> brandObservable = serverApi.getSearchBrand(700)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        brandObservable.subscribe(new Observer<List<PopularSearch>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<PopularSearch> popularSearches) {
                categorybrandlist.setValue(popularSearches);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString());
            }

            @Override
            public void onComplete() {

            }
        });



    }
}
