package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.Category;
import com.example.digikalaapp.model.CategoryTwo;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CategoryDigitalViewModel extends ViewModel {

    public MutableLiveData<List<CategoryTwo>> categorylist=new MutableLiveData<>();

    public MutableLiveData<List<CategoryTwo>> getcategoryList(){
        return categorylist;
    }

    public void getCategoryApi(){

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<CategoryTwo>> digitalObservable = serverApi.getCategory(200)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        digitalObservable.subscribe(new Observer<List<CategoryTwo>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<CategoryTwo> categoryTwos) {
                    categorylist.setValue(categoryTwos);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });


    }
}
