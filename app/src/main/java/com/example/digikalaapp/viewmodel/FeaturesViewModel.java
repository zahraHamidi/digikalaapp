package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.ProductDetails;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class FeaturesViewModel extends ViewModel {
    public MutableLiveData<List<ProductDetails>> productDetailsMutable = new MutableLiveData<>();
    public MutableLiveData<List<ProductDetails>> getProductDetailsMutable(){
        return  productDetailsMutable;
    }
    public void FeaturesApi(String name){
        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<ProductDetails>> productDetailsObservable = serverApi.showProductDetails(name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        productDetailsObservable.subscribe(new Observer<List<ProductDetails>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<ProductDetails> productDetails) {
                productDetailsMutable.setValue(productDetails);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString());
            }

            @Override
            public void onComplete() {

            }
        });

    }
}
