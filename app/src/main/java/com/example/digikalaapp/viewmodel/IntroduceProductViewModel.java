package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.Details;
import com.example.digikalaapp.model.Introduce;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class IntroduceProductViewModel extends ViewModel{


    public MutableLiveData<List<Introduce>> introduce = new MutableLiveData<>();
    public MutableLiveData<List<Introduce>> getIntroduceList(){
        return introduce;
    }

    public void introduceApi(String name){
        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<Introduce>> introduceObservable = serverApi.introduceProduct(name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        introduceObservable.subscribe(new Observer<List<Introduce>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<Introduce> introduces) {
                introduce.setValue(introduces);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });



    }


}
