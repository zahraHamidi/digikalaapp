package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class OfferOneViewModel extends ViewModel {

    public MutableLiveData<List<OfferOne>> offerOneList=new MutableLiveData<>();

    public MutableLiveData<List<OfferOne>> getOfferOneList(){
        return offerOneList;
    }

    public void getOfferOneApi(){

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<OfferOne>> offerOneObservable = serverApi.getOfferItem(1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        offerOneObservable.subscribe(new Observer<List<OfferOne>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<OfferOne> offerOnes) {
                offerOneList.setValue(offerOnes);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
