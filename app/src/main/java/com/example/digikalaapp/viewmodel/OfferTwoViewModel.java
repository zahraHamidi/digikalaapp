package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.OfferOne;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class OfferTwoViewModel extends ViewModel {

    public MutableLiveData<List<OfferOne>> offerTwoList=new MutableLiveData<>();

    public MutableLiveData<List<OfferOne>> getOfferTwoList(){
        return offerTwoList;
    }

    public void getOfferTwoApi(){

        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Observable<List<OfferOne>> offerTwoObservable = serverApi.getOfferItem(31)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        offerTwoObservable.subscribe(new Observer<List<OfferOne>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<OfferOne> offerTwoes) {
                offerTwoList.setValue(offerTwoes);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e("Error", "onError: "+e.toString() );
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
