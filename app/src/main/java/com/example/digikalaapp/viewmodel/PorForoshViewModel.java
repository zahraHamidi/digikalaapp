package com.example.digikalaapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.digikalaapp.model.PorForosh;
import com.example.digikalaapp.network.RetrofitInstance;
import com.example.digikalaapp.network.ServerApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PorForoshViewModel extends ViewModel {
    public MutableLiveData<List<PorForosh>> porForoshList = new MutableLiveData<>();

    public MutableLiveData<List<PorForosh>> getPorForoshList(){
        return porForoshList;
    }
    public void porForoshApi(){
        ServerApi serverApi = RetrofitInstance.getRetoInstance().create(ServerApi.class);
        Call<List<PorForosh>> call = serverApi.getItemPF(90);
        call.enqueue(new Callback<List<PorForosh>>() {
            @Override
            public void onResponse(Call<List<PorForosh>> call, Response<List<PorForosh>> response) {
                if (response.isSuccessful()) {
                    List<PorForosh> porForoshes = new ArrayList<>();
                    for (int i = 0; i <response.body().toArray().length ; i++) {
                        PorForosh porForosh = new PorForosh();
                        porForosh.setFid(i+1);
                        porForosh.setName(response.body().get(i).getName());
                        porForosh.setImage(response.body().get(i).getImage());
                        porForosh.setPrice(response.body().get(i).getPrice());
                        porForosh.setPre_price(response.body().get(i).getPre_price());
                        porForosh.setPercent(response.body().get(i).getPercent());
                        porForoshes.add(porForosh);

                    }
                    porForoshList.setValue(porForoshes);
                }
            }

            @Override
            public void onFailure(Call<List<PorForosh>> call, Throwable t) {
                Log.e("Error", "onFailure: "+t.toString() );

            }
        });
    }
}
